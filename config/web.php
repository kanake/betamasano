<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@bow'   => '@vendor/bower',
    ],
    'components' => [
           
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'joBCP2mXbdLJy8A1FLT8hTdme5JER_hM',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.onfonmedia.com',
                'username' => 'no_reply@onfonmedia.com',
                'password' => 'onfon@2015',
                'port' => '587',
                //'encryption' => 'tls',
            ],
            'useFileTransport' => true,
        ], 
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
$config['modules']['audit'] = [
        'class' => 'bedezign\yii2\audit\Audit',
        // the layout that should be applied for views within this module
        'layout' => 'main',
        // Name of the component to use for database access
        'db' => 'db',
        // List of actions to track. '*' is allowed as the last character to use as wildcard
        'trackActions' => ['*'],
        // Actions to ignore. '*' is allowed as the last character to use as wildcard (eg 'debug/*')
        'ignoreActions' => ['audit/*', 'debug/*'],
        // Maximum age (in days) of the audit entries before they are truncated
        //'maxAge' => 'debug',
        // IP address or list of IP addresses with access to the viewer, null for everyone (if the IP matches)
        //'accessIps' => ['127.0.0.1', '192.168.*'],
        // Role or list of roles with access to the viewer, null for everyone (if the user matches)
        'accessRoles' => ['admin'],
        // User ID or list of user IDs with access to the viewer, null for everyone (if the role matches)
        'accessUsers' => [1, 2],
        // Compress extra data generated or just keep in text? For people who don't like binary data in the DB
        'compressData' => true,
        // The callback to use to convert a user id into an identifier (username, email, ...). Can also be html.
        //'userIdentifierCallback' => ['app\models\User', 'userIdentifierCallback'],
        // If the value is a simple string, it is the identifier of an internal to activate (with default settings)
        // If the entry is a '<key>' => '<string>|<array>' it is a new panel. It can optionally override a core panel or add a new one.
        'panels' => [
            'audit/request',
            'audit/error',
            'audit/trail',
            'app/views' => [
                'class' => 'app\panels\ViewsPanel',
            // ...
            ],
        ],
        'panelsMerge' => [
        // ... merge data (see below)
        ]
    ];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
