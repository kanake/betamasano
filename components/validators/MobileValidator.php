<?php
namespace app\components\validators;

use yii\validators\Validator;

class MobileValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $numberRule[0] = array("netlen" => 2, "netNo" => 80, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Supercell sprl
        $numberRule[1] = array("netlen" => 2, "netNo" => 81, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Vodacom Congo RDC
        $numberRule[2] = array("netlen" => 2, "netNo" => 82, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Vodacom Congo RDC
        $numberRule[3] = array("netlen" => 2, "netNo" => 84, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Congo-Chine Telecom sarl (CCT)
        $numberRule[4] = array("netlen" => 2, "netNo" => 88, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Yozma Timeturns
        $numberRule[6] = array("netlen" => 2, "netNo" => 89, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Sait-Télécom sprl
        $numberRule[7] = array("netlen" => 2, "netNo" => 97, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); // Celtel Congo
        $numberRule[8] = array("netlen" => 2, "netNo" => 98, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); // Celtel Congo
        $numberRule[8] = array("netlen" => 2, "netNo" => 99, "prefix" => 243, "intprefix" => "+243", "localprefix" => "0", "userlen" => 7, "numlen" => 12); // Celtel Congo
	


        $userNumber =  0 + substr(str_replace(' ', '', $model->$attribute), -9);
        $len = strlen($userNumber);
        $mobileNumber = 0;
        $netNo = 0;
        $myRule = null;

        if ($userNumber > 800000000 and $userNumber < 999999999) {
            foreach ($numberRule as $rule) {
                $upperNet = 0;
                $netOK = true;
                $xs = $rule["userlen"] + $rule["netlen"];
                if ($len < $xs) {
                    $descr = "INVALID mobile number: $userNumber too short";
                    continue;
                }
                $mobileNumber = 0 + substr($userNumber, (-1 * $xs), $xs);
                $netNo = 0 + substr($userNumber, (-1 * $xs), $rule["netlen"]);

                if ($len >= $xs) {
                    $netOK = false;
                    $lx = $len - $xs;
                    if ($lx > 0) {
                        $upperNet = 0 + substr($userNumber, (-1 * ($lx + $xs)), $lx);
                    } else {
                        $upperNet = 0;
                    }

                    if ($upperNet == 0 or $upperNet == $rule["prefix"] or $upperNet == $rule["netNo"]
                        or $upperNet == $rule["localprefix"] or $upperNet == $rule["intprefix"]) {
                        $netOK = true;
                    }
                }

                if ($netNo == $rule["netNo"] and $netOK) {
                    $myRule = $rule;
                    break;
                }
            }
            if (is_null($myRule)) {
                $this->addError($model, $attribute, 'Mobile Number is iinvalid.');
            }

         } else {
            $this->addError($model, $attribute, 'Mobile Number is invalid.');
        }
    }
}