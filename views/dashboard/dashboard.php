<?php
use miloschuman\highcharts\Highcharts;
use yii\db\Query;
use yii\web\JsExpression;
use app\models\User;
use miloschuman\highcharts\HighchartsAsset;
use kartik\icons\Icon;

/* @var $this SiteController */
HighchartsAsset::register($this)->withScripts(['highcharts']);
HighchartsAsset::register($this)->withScripts(['highcharts-more']);
$this->title = 'DashBoard';
$connection = Yii::$app->db;

?>

<?php
$total_users = (new \yii\db\Query())
    ->select(['id'])
    ->from('user')
    ->count();
$companies = (new \yii\db\Query())
    ->select(['id'])
    ->from('user')
    ->where(['role_id' => 3])
    ->count();
$volunteers = (new \yii\db\Query())
    ->select(['id'])
    ->from('user_details')
    ->where(['is_volunteer' => 1])
    ->count();
$non_volunteers = (new \yii\db\Query())
    ->select(['id'])
    ->from('user_details')
    ->where(['is_volunteer' => 1])
    ->count();
?>
<div class="row">
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'user',
                'header' => 'Users',
                'text' => 'Total Users',
                'number' => $total_users,
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'briefcase',
                'header' => 'Companies',
                'text' => 'All registered Companies',
                'number' => $companies,
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'users',
                'header' => 'Volunteers',
                'text' => 'Volunteers',
                'number' => $volunteers,
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'users',
                'header' => 'None Volunteers',
                'text' => 'None Volunteers',
                'number' => $non_volunteers,
            ]
        )
        ?>
    </div>
   
</div>

<div class="row">
  <div class="col-xs-12 col-md-12">
      
      
    <?php
        $totalCalls = "SELECT COUNT(ud.id) as cnt, ifnull(ward, 'Others') as ward  "
                       . "FROM user_details ud "
                       . "INNER JOIN ward on ward.id = ud.ward_id  "
                       . "GROUP by ward_id ORDER BY ward.ward";

        $command = $connection->createCommand($totalCalls);

        $projects = $command->queryAll();
        $p = array();
        $name = array();
        foreach ($projects as $proj) {
            //$p[$proj['ward']] = (int)$proj['cnt'];
            //$p[] = ['name' => $proj['ward'], 'data' => [(int) $proj['cnt']]];
            $p[] = [$proj['ward'], (int) $proj['cnt']];
            $name[] = $proj['ward'];
        }

        echo Highcharts::widget([
                    'options' => [
                       'title' => ['text' => 'Professionals/Companies Per Ward'],
                       'xAxis' => [
                           'categories' => $name,
                          'title' => ['text' => 'Ward']
                       ],
                       'yAxis' => [
                          'title' => ['text' => 'Number of Professionals/Companies']
                       ],
                       //'colors' => ['#468847', '#ed1b24', '#ff5500', '#999999'],
                       'gradient' => ['enabled' => true],
                       'credits' => ['enabled' => false],
                       'exporting' => ['enabled' => false],
                       'chart' => [
                            'plotBackgroundColor' => '#ffffff',
                            'plotBorderWidth' => null,
                            'plotShadow' => false,
                            'height' => 400,
                            'type' => 'column'
                        ],
                       'series' => [['name' => 'Wards', 'data' => $p]]
                    ]
                 ]);
   ?>

  </div>

</div>
