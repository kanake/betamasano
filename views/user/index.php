<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use \yiister\gentelella\widgets\Panel;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12 col-xs-12">
    <?php Panel::begin();?>

    <?= \yiister\gentelella\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [ 
                'attribute' => 'role_id',
                'label' => 'Role',
                'format' => 'text',
                'content' => function($data) {
                    return $data->getRoleName();
                }
            ],
            'username',
            'email:email',
            'msisdn',
            [ 
                'attribute' => 'status_id',
                'label' => 'Status',
                'format' => 'text',
                'content' => function($data) {
                    return $data->getStatusName();
                }
            ],        
                    
            'date_created',
            // 'createdBy',
            [ 
                'attribute' => 'created_by',
                'label' => 'Created By',
                'format' => 'text',
                'content' => function($data) {
                    return $data->getCreatedBy();
                }
            ],
            // 'dateModified',
            // 'modifiedBy',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{reset}{update}',
                'buttons' => [
                    'reset' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                    'title' => Yii::t('app', 'Reset PIN'),                            
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'reset') {
                        return  Url::to(['user/reset-password', 'id' => $model['id']]);
                    }
                }
            ],
        ],
    ]); ?>

 <?php Panel::end() ?>
</div>
</div>