<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use \yiister\gentelella\widgets\Panel;

$this->title = 'Registration Complete';
?>
 
 <?= \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true]);?>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <?php
        Panel::begin();
        Panel::begin(['header' => 'Personal Details',]);
        echo DetailView::widget([
            'model' => $individual,
            'attributes' => [
                'salutationName',
                'first_name',
                'last_name',
                'genderValue',
                'date_of_birth',
                'id_number',
                'phone_number',
                'volunteer',
                'wardName',
                [
                    'label' => 'Company Profile',
                    'attribute'=>'curriculum_vitae',
                    'format'=>'raw',
                    'value'=>Html::a($individual->curriculum_vitae, ['user/download', 'filename' => $individual->curriculum_vitae]),
                ],
            ]
        ]);
        Panel::end();
        Panel::begin(['header' => 'Company Details',]);
            echo DetailView::widget([
                'model' => $company,
                'attributes' => [
                    'sectorName',
                    'company_name',
                    'phone_number',
                    'address',
                    'postal_code',
                    'city',
                    'countyName',
                    'email',
                    'domain',
                ]
            ]);
        Panel::end();    
        

/*[
                'attribute'=>'upload',
                'format'=>'raw',
                'value'=>Html::a($project->upload, ['project/download', 'fileName' => $project->upload]),
            ],*/
        Panel::end();
        ?>
</div>
</div>