<?php
use yii\helpers\Html;
use \yiister\gentelella\widgets\Panel;
$this->title = 'Invalid Step';

?>
<div class="row">
    <div class="col-md-12 col-xs-12">
    <?php
        Panel::begin([
                'header' => "Invalid step",
            ]
            );
        echo Html::tag('div', strtr('An invalid step ({step}) was detected.', [
            '{step}' => $event->step
        ]));
Panel::end() ?>
</div>
</div>