
<?php

use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DepDrop;
use kartik\helpers\Html;
use kartik\builder\Form;
use beastbytes\wizard\WizardMenu;
use \yiister\gentelella\widgets\Panel;
use kartik\tabs\TabsX;
$this->title = 'Personal Details';
/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */


?>
<div class="row">
    <div class="col-md-12 col-xs-12">
<?php Panel::begin();?>

<?php echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);?>

<div class="row">
<?php
Panel::begin(
             [
                'header' => "Personal Details",
            ]
            );
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 
    'fullSpan' => 10, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL], 'options' => ['enctype' => 'multipart/form-data']]);

echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['class' => 'col-md-3'],
                'inputContainer' => ['class' => 'col-md-9'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => [
                'salutation' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => $model->getSalutationList(),
                    'options' => ['prompt' => '-None-']
                ],
                'first_name' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'First Name']
                ],
                'last_name' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'Last Name']
                ],
                'gender' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => $model->getGenderList(),
                    'options' => ['prompt' => '-None-']
                ],
                

                'date_of_birth' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\widgets\DatePicker',
                    'options' => [
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
                            'startDate' =>  date('Y-m-d'),
                            //'todayHighlight' => true
                        ],
                        'options' => ['placeholder' => 'Date of Birth'],
                        //'columnOptions'=>['colspan'=>1],
                    ],
                //'hint'=>'Enter birthday (mm/dd/yyyy)'
                ],
                
                'id_number' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'ID Number']
                ],
                'phone_number' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'Phone Number']
                ],
                'is_volunteer' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => $model->getVolunteerList(),
                    'options' => ['prompt' => '-None-']
                ],
                'ward' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => $model->getWardList(),
                    'options' => ['prompt' => '-None-']
                ],
                'actions' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<ul class="list-inline pull-right"><li>' . 
                    Html::submitButton('Prev', ['class' => 'btn btn-primary', 'name' => 'prev', 'value' => 'prev']) . '</li><li>' .
                    Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'next', 'value' => 'next']) . '</li><li>' .
                    Html::submitButton('Pause', ['class' => 'btn btn-primary', 'name' => 'pause', 'value' => 'pause']) . '</li><li>' .
                    Html::submitButton('Cancel', ['class' => 'btn btn-primary', 'name' => 'cancel', 'value' => 'pause']) . '</li></ul>'
                ],
           ],
        ]);


ActiveForm::end();
Panel::end();
?>
</div>
 <?php Panel::end() ?>
</div>
</div>

