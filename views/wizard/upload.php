<?php

use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DepDrop;
use kartik\helpers\Html;
use kartik\builder\Form;
use beastbytes\wizard\WizardMenu;
use \yiister\gentelella\widgets\Panel;

$this->title = 'Curriculum Vitae';
/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12 col-xs-12">
<?php Panel::begin();?>

<?php echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);?>

<?php
Panel::begin([
                'header' => "Upload Curriculum Vitae",
            ]
            );
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 
    'fullSpan' => 10, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL], 'options' => ['enctype' => 'multipart/form-data']]);
    echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['class' => 'col-md-3'],
                'inputContainer' => ['class' => 'col-md-9'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => [
                'upload' => [
                    'type' => Form::INPUT_FILE, 
                    //'options' => ['placeholder' => 'Mobile No']
                ],
                'curriculum_vitae' => [
                    'type'=>Form::INPUT_HIDDEN, 
                    'columnOptions'=>['hidden'=>true]
                ],
                'actions' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<ul class="list-inline pull-right"><li>' .
                    Html::submitButton('Prev', ['class' => 'btn btn-primary', 'name' => 'prev', 'value' => 'prev']) . '</li><li>' .
                    Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'next', 'value' => 'next']) . '</li><li>' .
                    Html::submitButton('Pause', ['class' => 'btn btn-primary', 'name' => 'pause', 'value' => 'pause']) . '</li><li>' .
                    Html::submitButton('Cancel', ['class' => 'btn btn-primary', 'name' => 'cancel', 'value' => 'pause']) . '</li></ul>'
                ],
           ],
        ]);

ActiveForm::end();
 Panel::end();
?>
 <?php Panel::end() ?>
</div>
</div>
