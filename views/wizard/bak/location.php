
<?php

use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DepDrop;
use kartik\helpers\Html;
use kartik\builder\Form;
use beastbytes\wizard\WizardMenu;

$this->title = 'Project Wizard';
/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile("@web/css/wizard.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');

?>

<div class="wizard">

    <div class="wizard-inner">
        <div class="connecting-line"></div>
        <?php echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);?>
    </div>
    <div class="tab-content">
        <div class="tab-pane active">
<?php
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 
    'fullSpan' => 10, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL], 'options' => ['enctype' => 'multipart/form-data']]);
echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['class' => 'col-md-3'],
                'inputContainer' => ['class' => 'col-md-9'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => [
                'county_id' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => $model->getCountyList(),
                    'options' => ['prompt' => 'Select Constituency ..', 'id' => 'county-id']
                ],
                'constituency_id' => [
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'kartik\widgets\DepDrop', 
                    'options' => [
                        'data'=> $model->getConstituencyList(),
                        'options' => ['placeholder' => 'Select Constituency...', 'id'=>'contituency-id'],
                        'pluginOptions'=>[
                            'depends'=>['county-id'],
                            'placeholder'=>'Select Constituency..',
                            'url'=>Url::to(['/project/constituency'])
                        ]
                    ],
                ],
                'ward_id'=>[
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'kartik\widgets\DepDrop', 
                    'options' => [
                        'data'=> $model->getWardList(),
                        'options' => ['placeholder' => 'Select Ward ...', 'id'=>'ward-id'],
                        'pluginOptions'=>[
                            'depends'=>['contituency-id'],
                            'placeholder'=>'Select Ward...',
                            'url'=>Url::to(['/project/ward'])
                        ]
                    ],
                ],
                'polling_center_id'=>[
                    'type'=>Form::INPUT_WIDGET, 
                    'widgetClass'=>'kartik\widgets\DepDrop', 
                    'options' => [
                        'data'=> $model->getPollingCenterList(),
                        'options' => ['placeholder' => 'Select Polling Center ...', 'id'=>'center-id'],
                        'pluginOptions'=>[
                            'depends'=>['ward-id'],
                            'placeholder'=>'Select Polling Center...',
                            'url'=>Url::to(['/project/center'])
                        ]
                    ],
                ],
                'actions' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<ul class="list-inline pull-right"><li>' .
                    Html::submitButton('Prev', ['class' => 'btn btn-primary', 'name' => 'prev', 'value' => 'prev']) . '</li><li>' .
                    Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'next', 'value' => 'next']) . '</li><li>' .
                    Html::submitButton('Pause', ['class' => 'btn btn-primary', 'name' => 'pause', 'value' => 'pause']) . '</li><li>' .
                    Html::submitButton('Cancel', ['class' => 'btn btn-primary', 'name' => 'cancel', 'value' => 'pause']) . '</li></ul>'
                ],
           ],
        ]);

/*echo Html::panel(
        ['heading' => $this->title, 'body' => '<div class="panel-body">' . $content . '</div>'], 
        Html::TYPE_PRIMARY
);
*/
echo Html::endForm();
?>
</div>
</div>
</div>