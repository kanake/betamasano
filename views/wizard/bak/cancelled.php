<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Wizard Cancelled';

echo Html::tag('h1', $this->title);
echo Html::tag('div', 'Project Details not saved');
