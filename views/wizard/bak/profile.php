
<?php

use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DepDrop;
use kartik\helpers\Html;
use kartik\builder\Form;
use beastbytes\wizard\WizardMenu;

$this->title = 'Project Wizard';
/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
$this->registerCssFile("@web/css/wizard.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');

?>

<div class="wizard">

    <div class="wizard-inner">
        <div class="connecting-line"></div>
        <?php echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);?>
    </div>
    <div class="tab-content">
        <div class="tab-pane active">
<?php
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 
    'fullSpan' => 10, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL], 'options' => ['enctype' => 'multipart/form-data']]);
echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['class' => 'col-md-3'],
                'inputContainer' => ['class' => 'col-md-9'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => [
               'name' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'Your Name']
                ],
                'msisdn' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'Telephone Number']
                ],
                'email' => [
                    'type' => Form::INPUT_TEXT, 
                    'options' => ['placeholder' => 'Email Address']
                ],
                'actions' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<ul class="list-inline pull-right"><li>' .
                    Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'next', 'value' => 'next']) . '</li><li>' .
                    Html::submitButton('Pause', ['class' => 'btn btn-primary', 'name' => 'pause', 'value' => 'pause']) . '</li><li>' .
                    Html::submitButton('Cancel', ['class' => 'btn btn-primary', 'name' => 'cancel', 'value' => 'pause']) . '</li></ul>'
                ],
           ],
        ]);

echo Html::endForm();
?>
</div>
</div>
</div>