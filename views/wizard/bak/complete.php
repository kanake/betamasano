<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Registration Wizard Complete';

//echo $event->sender->menu->run();

echo Html::beginTag('div', ['class' => 'section']);
echo Html::tag('h2', 'Profile');
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'name',
        'msisdn',
        'email',
    ]
]);
echo Html::endTag('div');

echo Html::beginTag('div', ['class' => 'section']);
echo Html::tag('h2', 'Location');
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'countyName',
        'constituencyName',
        'wardName',
        'pollingCenterName'
    ]
]);


echo Html::endTag('div');

echo Html::beginTag('div', ['class' => 'section']);
echo Html::tag('h2', 'Project');
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'category_id',
        'title',
        'description',
        'proposed_solution',
        'available_facilities',
        [
            'attribute'=>'upload',
            'format'=>'raw',
            'value'=>Html::a($model->upload, ['project/download', 'fileName' => $model->upload]),
        ],
    ]
]);

