
<?php

use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DepDrop;
use kartik\helpers\Html;
use kartik\builder\Form;
use beastbytes\wizard\WizardMenu;
use \yiister\gentelella\widgets\Panel;
use wbraganca\dynamicform\DynamicFormWidget;

$this->title = 'Qualification';
/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */

$js = '

jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {

        jQuery(this).html("Qualification: " + (index + 1))

    });

});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {

    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {

        jQuery(this).html("Qualification: " + (index + 1))

    });

});

';


$this->registerJs($js);

?>
<div class="row">
    <div class="col-md-12 col-xs-12">
<?php Panel::begin();?>

<?php echo WizardMenu::widget(['step' => $event->step, 'wizard' => $event->sender]);?>

<?php

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL,   'id' => 'dynamic-form',
    'fullSpan' => 10, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL], 'options' => ['enctype' => 'multipart/form-data']]);

 DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 5, // the maximum times, an element can be cloned (default 999)
        'min' => 1, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.fa-close', // css class
        'model' => $models[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'degree_level',
            'course_of_study',
        ],
    ]);
?>

<div class="container-items">
<div class="item">

<?php
    
    foreach ($models as $index => $model) {
        Panel::begin(
             [
                'header' => "<div class='panel-title'>Qualification : " . $index . '</div>',
                'tools' => [
                    [
                        'encode' => false,
                        'items' => [],
                        'label' => new \rmrevin\yii\fontawesome\component\Icon('close'),
                    ],
                ]
            ]
            );
        echo $content = Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['class' => 'col-md-5'],
                'inputContainer' => ['class' => 'item col-md-7'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => [
                '[' . $index .']sector' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => $model->getSectorList(),
                    'options' => ['prompt' => '-None-'],
                    'label' => 'Sector',
                    //'hint' => '',
                ],
                '[' . $index . ']organisation_name' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Organisation Name'],
                    'label' => 'Organisation Name',
                ],
                '[' . $index . ']job_title' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Job Title'],
                    'label' => 'Job Title',
                ],
                '[' . $index . ']years_experience' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Years of Experience in this position'],
                    'label' => 'Years of Experience',
                ],
            ],
        ]);

        $index =+ 1;
        Panel::end();
    }
    
?>
</div>
</div>

<?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'actions' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<ul class="list-inline pull-right"><li>' .
                   //<button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add Project</button>
                    Html::Button('Add Qualification', ['class' => 'add-item btn btn-primary', 'name' => 'add', 'value' => 'add']) . '</li><li>' .
                    Html::submitButton('Prev', ['class' => 'btn btn-primary', 'name' => 'prev', 'value' => 'prev']) . '</li><li>' .
                    Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'next', 'value' => 'next']) . '</li><li>' .
                   // Html::submitButton('Pause', ['class' => 'btn btn-primary', 'name' => 'pause', 'value' => 'pause']) . '</li><li>' .
                    Html::submitButton('Cancel', ['class' => 'btn btn-primary', 'name' => 'cancel', 'value' => 'pause']) . '</li></ul>'
                ],

             ],
        ]);

    DynamicFormWidget::end();  
    ActiveForm::end();      
    
     
      
    Panel::end(); 

    //echo Html::endForm();
    
?>
</div>
</div>



