<?php

namespace app\libraries;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Crypto {

    private $cipher = "rijndael-128";
    private $mode = "cbc";
    private $secret_key = "D4:6E:AC:3F:F0:BE";
    //iv length should be 16 bytes 
    private $iv = "fedcba9876543210";

    function __construct() {
        $keyLen = strlen($this->secret_key);
        if ($keyLen < 16) {
            $addS = 16 - $keyLen;
            for ($i = 0; $i < $addS; $i++) {
                $this->secret_key.=" ";
            }
        } else {
            $this->secret_key = substr($this->secret_key, 0, 16);
        }
    }

    public function encrypt($str) {

        $td = mcrypt_module_open($this->cipher, "", $this->mode, $this->iv);
        mcrypt_generic_init($td, $this->secret_key, $this->iv);
        $cyper_text = mcrypt_generic($td, $str);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return bin2hex($cyper_text);
    }

    public function decrypt($str) {
        $td = mcrypt_module_open($this->cipher, "", $this->mode, $this->iv);
        mcrypt_generic_init($td, $this->secret_key, $this->iv);
        $decrypted_text = mdecrypt_generic($td, hex2bin($str));
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return trim($decrypted_text);
    }

}
