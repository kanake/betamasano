<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\components\validators\MobileValidator;

class Company extends Model
{
    public $sector;
    public $company_name;
    public $address;
    public $county;
    public $city;
    public $postal_code;
    public $phone_number;
    public $email;
    public $domain;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['sector', 'company_name', 'county', 'city', 'phone_number'], 'required'],
            [['address', 'postal_code', 'domain'],  'safe'],
            ['email', 'email'],
            ['phone_number', MobileValidator::className()],    
        ];
    }

    public function attributeLabels()
    {
        return [
            'is_volunteer' => 'Volunteer',
        ];
    }

    public function getSectorList() {
        return ArrayHelper::map(Sector::find()->all(),'id','sector'); 
    }

    public function getCountyList() {
        return ArrayHelper::map(County::find()->all(), 'id','county_name'); 
    }
    public function getSectorName() {
        $sector = Sector::findOne($this->sector);
        return $sector ? $sector->sector : "";
    }    

    public function getCountyName() {
        $county = County::findOne($this->county);
        return $county ? $county->county_name : "";
    }   
}