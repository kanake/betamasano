<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property int $user_id
 * @property int $sector_id
 * @property string $company_name
 * @property string $phone_number
 * @property string $address
 * @property string $postal_code
 * @property string $city
 * @property int $county_id
 * @property string $email
 * @property string $domain
 * @property string $date_created
 * @property string $created_by
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'sector_id', 'county_id'], 'integer'],
            [['date_created', 'created_by'], 'safe'],
            [['company_name', 'address', 'domain'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 12],
            [['postal_code'], 'string', 'max' => 20],
            [['city', 'email'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sector_id' => 'Sector ID',
            'company_name' => 'Company Name',
            'phone_number' => 'Phone Number',
            'address' => 'Address',
            'postal_code' => 'Postal Code',
            'city' => 'City',
            'county_id' => 'County ID',
            'email' => 'Email',
            'domain' => 'Domain',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        ];
    }
    public function getSectorName() {
        $sector = Sector::findOne($this->sector_id);
        return $sector ? $sector->sector : "";
    }    

    public function getCountyName() {
        $county = County::findOne($this->county_id);
        return $county ? $county->county_name : "";
    }   
}
