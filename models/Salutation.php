<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "salutation".
 *
 * @property int $id
 * @property string $salutation
 */
class Salutation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'salutation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['salutation'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'salutation' => 'Salutation',
        ];
    }
}
