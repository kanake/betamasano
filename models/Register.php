<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

class Register extends Model
{
    public $reg_type;
    public $email;
    public $password;
    public $repeat_password;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['reg_type', 'email', 'password', 'repeat_password' ], 'required'],
            //['email', 'unique'],
            ['email', 'email'],
            ['repeat_password', 'compare', 'compareAttribute' => 'password'],
            ['email', 'validate_email'],
        ];
    }

    public function validate_email($attribute, $params, $validator)
    {
        $user = User::find()->where('email =:email', [':email' => $this->$attribute])->one();
        if ($user) {
             $this->addError($attribute, 'This email address is already registered on the system');
        }

    }

    public function attributeLabels()
    {
        return [
            'reg_type' => 'Registration Type',
        ];
    }

}


