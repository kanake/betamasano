<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qualification".
 *
 * @property int $id
 * @property int $user_id
 * @property int $sector_id
 * @property string $organisation_name
 * @property string $job_title
 * @property string $years_experience
 * @property string $date_created
 * @property int $created_by
 */
class Qualifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qualification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sector_id', 'created_by'], 'integer'],
            [['date_created'], 'safe'],
            [['organisation_name'], 'string', 'max' => 20],
            [['job_title'], 'string', 'max' => 50],
            [['years_experience'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sector_id' => 'Sector ID',
            'organisation_name' => 'Organisation Name',
            'job_title' => 'Job Title',
            'years_experience' => 'Years Experience',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        ];
    }
    
    public function getSectorList() {
        return ArrayHelper::map(Sector::find()->all(), 'id', 'sector'); 
    }

    public function getSectorName() {
        $sector = Sector::findOne($this->sector_id);
        return $sector ? $sector->sector : "";
    }


}
