<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class Profile extends Model
{
    public $upload;
    public $company_profile;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            //[['category_id',  'title', 'description', 'proposed_solution', 'available_facilities'], 'required'],
            [['upload'], 'file'],
            [['company_profile'], 'safe'],
        ];
    }

    public function upload()
    {
        if (isset($this->upload)) {
            $this->company_profile = $this->upload->baseName . '.' . $this->upload->extension;
            $this->upload->saveAs('uploads/' . $this->upload->baseName . '.' . $this->upload->extension);
        } 
        return true;

    }

}
