<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class Upload extends Model
{
    public $upload;
    public $curriculum_vitae;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            //[['category_id',  'title', 'description', 'proposed_solution', 'available_facilities'], 'required'],
            [['upload'], 'file'],
            [['curriculum_vitae'], 'safe'],
        ];
    }

    public function upload()
    {
        if (isset($this->upload)) {
            $this->curriculum_vitae = $this->upload->baseName . '.' . $this->upload->extension;
            $this->upload->saveAs('uploads/' . $this->upload->baseName . '.' . $this->upload->extension);
        } 
        return true;

    }

}
