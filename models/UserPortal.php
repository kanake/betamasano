<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_portal".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property integer $role_id
 * @property string $username
 * @property string $email
 * @property string $msisdn
 * @property string $password
 * @property string $salt
 * @property string $activation_key
 * @property integer $status_id
 * @property string $date_created
 * @property integer $created_by
 * @property string $date_modified
 * @property integer $modified_by
 */
class UserPortal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_portal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'status_id', 'created_by', 'modified_by'], 'integer'],
            [['password', 'salt', 'activation_key'], 'string'],
            [['date_created', 'date_modified'], 'safe'],
            [['firstname', 'username', 'email'], 'string', 'max' => 30],
            [['lastname'], 'string', 'max' => 20],
            [['msisdn'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'role_id' => 'Role ID',
            'username' => 'Username',
            'email' => 'Email',
            'msisdn' => 'Msisdn',
            'password' => 'Password',
            'salt' => 'Salt',
            'activation_key' => 'Activation Key',
            'status_id' => 'Status ID',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'date_modified' => 'Date Modified',
            'modified_by' => 'Modified By',
        ];
    }
}
