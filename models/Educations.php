<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "education".
 *
 * @property int $id
 * @property int $user_id
 * @property int $degree_level_id
 * @property int $course_of_study_id
 * @property string $date_created
 * @property int $created_by
 */
class Educations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'degree_level_id', 'course_of_study_id', 'created_by'], 'integer'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'degree_level_id' => 'Degree Level ID',
            'course_of_study_id' => 'Course Of Study ID',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        ];
    }
    public function getDegreeLevelList() {
        return ArrayHelper::map(DegreeLevel::find()->all(),'id','degree_level'); 
    }

    public function getCourseList() {
        return ArrayHelper::map(Course::find()->all(),'id','course'); 
    }
    public function getDegreeLevelName() {
        $degree = DegreeLevel::findOne($this->degree_level_id);
        return $degree ? $degree->degree_level : "";
    }

    public function getCourseName() {
        $course = Course::findOne($this->course_of_study_id);
        return $course ? $course->course : "";
    }
}
