<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

class Qualification extends Model
{
    public $sector;
    public $organisation_name;
    public $job_title;
    public $years_experience;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['sector', 'organisation_name', 'job_title', 'years_experience' ], 'required'],
        ];
    }

    public function getSectorList() {
        return ArrayHelper::map(Sector::find()->all(),'id','sector'); 
    }

    public function attributeLabels()
    {
        return [
            'years_experience' => 'Years of work experience : ',
        ];
    }

    public function getSectorName() {
        $sector = Sector::findOne($this->sector);
        return $sector ? $sector->sector : "";
    }


}