<?php

namespace app\models;

use Yii;
use app\libraries\Encryption;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

use app\components\validators\MobileValidator;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property integer $clientId
 * @property string $roleId
 * @property string $username
 * @property string $email
 * @property string $msisdn
 * @property string $password
 * @property string $salt
 * @property string $activationKey
 * @property integer $status_id
 * @property string $dateCreated
 * @property integer $createdBy
 * @property string $dateModified
 * @property integer $modifiedBy
 *
 * @property UserMessage[] $userMessages
 * @property UserService[] $userServices
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {
    
    const ROLE_ADMIN = 1;
    const ROLE_FINANCE = 2;
    const ROLE_GAMING = 3;
    const ROLE_MARKETING = 4;
    const ROLE_CCR = 5;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    
    const SCENARIO_CREATE = 1;
    const SCENARIO_CHANGEPASSWORD = 2;
    const SCENARIO_UPDATE = 3;
    const SCENARIO_RESET = 4;
    
    public $repeat_password;
    public $current_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_portal';
    }
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'role_id', 'username', 'email', 'msisdn',  'password', 'repeat_password', 'salt', 'status_id'], 'required', 'on' => self::SCENARIO_CREATE],
            [['current_password', 'password', 'repeat_password'], 'required', 'on' =>  self::SCENARIO_CHANGEPASSWORD],
            [['role_id', 'username', 'email', 'msisdn', 'current_password', 'password', 'repeat_password'], 'required', 'on' =>  self::SCENARIO_UPDATE],
            [['password', 'repeat_password'], 'required', 'on' =>  self::SCENARIO_RESET],
            [['role_id', 'status_id', 'created_by', 'modified_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['username'], 'string', 'max' => 20],
            [['firstname', 'lastname'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 45],
            [['msisdn'], 'string', 'max' => 12],
            [['password', 'salt'], 'string', 'max' => 255],
            [['activation_key'], 'string', 'max' => 128],
            [['username', 'email'], 'unique'],
             [['email'], 'email'],
            ['msisdn', MobileValidator::className()],
            ['repeat_password', 'compare', 'compareAttribute' => 'password'],
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_created', 'date_modified'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'date_modified',
                ],
                'value' => new Expression('NOW()'),
            ],
             'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }
    
   

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'username' => 'Username',
            'email' => 'Email',
            'msisdn' => 'Msisdn',
            'password' => 'Password',
            'salt' => 'Salt',
            'activation_key' => 'Activation Key',
            'status_id' => 'Status ID',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'date_modified' => 'Date Modified',
            'modified_by' => 'Modified By',
        ];
    }

   
    
    public static function findByUsername($username) {
        $user = User::find()->where('username =:username', [':username' => $username])->one();
        if ($user)
            return $user;
        return null;
    }

    public function validatePassword($password, $goodhash, $salt) {
        return Encryption::validate_password($password, $goodhash, $salt);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        return User::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

    public function getRoleName() {
        $role = Role::findOne($this->role_id);
        return $role ? $role->role : "";
    }
    
    public function getCreatedBy() {
        $user = User::findOne($this->created_by);
        return $user ? $user->username : "";
    }
    
    public function getStatusName() {
        $status = Status::findOne($this->status_id);
        return $status ? $status->status_value : "";
    }
    
    public function getRoleList() {
        
        //return ArrayHelper::map(Role::find()->where('id > ' .Yii::$app->user->identity->roleId)->all(),'id','role');
        
        return ArrayHelper::map(Role::find()->all(),'id','role'); 
    }
    
    public static function activate($email, $key) {
        if ($user = User::find()->where(["email" => $email])->one()) {
            if ($user) {
                if ($user->status_id != self::STATUS_INACTIVE)
                    return -1;
                if ($user->activationKey == $key) {
                    $user->activationKey = "1";
                    $user->status_id = self::STATUS_ACTIVE;
                    if ($user->update('safe')) {
                            return $user;
                    } 
                } else return -2;
            } else return -3;
        }
        return false;
    }
}
