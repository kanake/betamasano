<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property int $id
 * @property int $user_id
 * @property int $salutation_id
 * @property string $first_name
 * @property string $last_name
 * @property int $gender_id
 * @property string $date_of_birth
 * @property string $id_number
 * @property string $phone_number
 * @property int $is_volunteer
 * @property int $ward_id
 * @property string $curriculum_vitae
 * @property string $date_created
 * @property int $created_by
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'salutation_id', 'gender_id', 'is_volunteer', 'ward_id', 'created_by'], 'integer'],
            [['date_of_birth', 'date_created'], 'safe'],
            [['first_name'], 'string', 'max' => 30],
            [['last_name'], 'string', 'max' => 40],
            [['id_number'], 'string', 'max' => 20],
            [['phone_number'], 'string', 'max' => 12],
            [['curriculum_vitae'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'salutation_id' => 'Salutation ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'gender_id' => 'Gender ID',
            'date_of_birth' => 'Date Of Birth',
            'id_number' => 'Id Number',
            'phone_number' => 'Phone Number',
            'is_volunteer' => 'Is Volunteer',
            'ward_id' => 'Ward ID',
            'curriculum_vitae' => 'Curriculum Vitae',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        ];
    }
    public function getSalutationName() {
        $salutation = Salutation::findOne($this->salutation_id);
        return $salutation ? $salutation->salutation : "";
    }

    public function getVolunteer() {
        $volunteer = Volunteer::findOne($this->is_volunteer);
        return $volunteer ? $volunteer->volunteer : "";
    }

    public function getWardName() {
        $ward = Ward::findOne($this->ward_id);
        return $ward ? $ward->ward : "";
    }

    public function getGenderValue() {
        $gender = Gender::findOne($this->gender_id);
        return $gender ? $gender->gender : "";
    }

}
