<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "degree_level".
 *
 * @property int $id
 * @property string $degree_level
 */
class DegreeLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'degree_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['degree_level'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'degree_level' => 'Degree Level',
        ];
    }
}
