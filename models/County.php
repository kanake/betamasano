<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "county".
 *
 * @property int $id
 * @property string $county_name
 */
class County extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'county';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['county_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'county_name' => 'County Name',
        ];
    }
}
