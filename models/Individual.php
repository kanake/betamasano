<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\components\validators\MobileValidator;


class Individual extends Model
{
    public $salutation;
    public $first_name;
    public $last_name;
    public $gender;
    public $date_of_birth;
    public $id_number;
    public $phone_number;
    public $is_volunteer;
    public $ward;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['salutation', 'first_name', 'last_name', 'gender', 'date_of_birth', 'id_number', 'phone_number', 'is_volunteer', 'ward' ], 'required'],
            ['phone_number', MobileValidator::className()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'is_volunteer' => 'Volunteer',
        ];
    }

    public function getSalutationList() {
        return ArrayHelper::map(Salutation::find()->all(),'id','salutation'); 
    }

    public function getGenderList() {
        return ArrayHelper::map(Gender::find()->all(),'id','gender'); 
    }

    public function getVolunteerList() {
        return ArrayHelper::map(Volunteer::find()->all(),'id','volunteer'); 
    }

    public function getWardList() {
        return ArrayHelper::map(Ward::find()->orderBy(['ward'=>SORT_DESC])->all(),'id','ward'); 
    }

    public function getSalutationName() {
        $salutation = Salutation::findOne($this->salutation);
        return $salutation ? $salutation->salutation : "";
    }

    public function getVolunteer() {
        $volunteer = Volunteer::findOne($this->is_volunteer);
        return $volunteer ? $volunteer->volunteer : "";
    }

    public function getWardName() {
        $ward = Ward::findOne($this->ward);
        return $ward ? $ward->ward : "";
    }

    public function getGenderValue() {
        $gender = Gender::findOne($this->gender);
        return $gender ? $gender->gender : "";
    }

}