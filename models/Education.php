<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

class Education extends Model
{
    public $degree_level;
    public $course_of_study;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['degree_level', 'course_of_study' ], 'required'],
        ];
    }

    public function getDegreeLevelList() {
        return ArrayHelper::map(DegreeLevel::find()->all(),'id','degree_level'); 
    }

    public function getCourseList() {
        return ArrayHelper::map(Course::find()->all(),'id','course'); 
    }
    public function getDegreeLevelName() {
        $degree = DegreeLevel::findOne($this->degree_level);
        return $degree ? $degree->degree_level : "";
    }

    public function getCourseName() {
        $course = Course::findOne($this->course_of_study);
        return $course ? $course->course : "";
    }
}