<?php

namespace app\controllers;

use Yii;
use app\models\Model;
use yii\web\Controller;
use beastbytes\wizard\WizardBehavior;
use yii\web\UploadedFile;
use app\models\Register;
use app\models\Individual;
use app\models\Education;
use app\models\Qualification;
use app\libraries\Encryption;

class WizardController extends Controller
{

    public function beforeAction($action)
    {
        $config = [];
        switch ($action->id) {
            case 'registration':
                $config = [
                    'steps' => [
                        'register',
                        [
                            'personal' => [
                                'individual', 'education', 'qualification', 'upload', 
                            ],
                            'organisation' => [
                                'individual', 'company', 'profile'
                            ]
                        ],
                    ],
                    'autoAdvance'   => false,
                    'defaultBranch' => false,
                    'events' => [
                        WizardBehavior::EVENT_WIZARD_STEP => [$this, $action->id.'WizardStep'],
                        WizardBehavior::EVENT_AFTER_WIZARD => [$this, $action->id.'AfterWizard'],
                        WizardBehavior::EVENT_INVALID_STEP => [$this, 'invalidStep']
                    ]
                ];
                break;
            case 'resume':
                $config = ['steps' => []]; // force attachment of WizardBehavior
            default:
                break;
        }
        if (!empty($config)) {
            $config['class'] = WizardBehavior::className();
            $this->attachBehavior('wizard', $config);
        }
        return parent::beforeAction($action);
    }

    public function actionRegistration($step = null)
    {
        //if ($step===null) $this->resetWizard();
        return $this->step($step);
    }

    /**
    * Process wizard steps.
    * The event handler must set $event->handled=true for the wizard to continue
    * @param WizardEvent The event
    */
    public function registrationWizardStep($event)
    {

        if (empty($event->stepData)) {
            $modelName = 'app\\models\\'.ucfirst($event->step);
            if ($event->step == "education" || $event->step == "qualification" ) {
                $models = [new $modelName()];
            } else {
                $model = new $modelName();
            }

            $model = new $modelName();
        } else {
            if ($event->step == "education" || $event->step == "qualification" ) {
                $models = $event->stepData;
            } else {
                $model = $event->stepData;
            }
        }
        $post = Yii::$app->request->post();
        if (isset($model) && $model->formName() == "Upload" ) {
            $model->upload = UploadedFile::getInstance($model, 'upload');
            $model->upload();
            if (isset($model->curriculum_vitae)) {
                $post['Upload']['curriculum_vitae'] = $model->curriculum_vitae;
            }
        }

        if (isset($model) && $model->formName() == "Profile") {
            $model->upload = UploadedFile::getInstance($model, 'upload');
            $model->upload();
           
            if (isset($model->company_profile)) {
                $post['Upload']['company_profile'] = $model->company_profile;
            } 
        }

        switch ($event->step) {

            case 'register':
                $model->load($post);
                $event->branches = ($model->reg_type == 1
                    ? [
                        'personal' => WizardBehavior::BRANCH_SELECT,
                        'organisation'  => WizardBehavior::BRANCH_DESELECT,
                    ]
                    : [
                        'personal' => WizardBehavior::BRANCH_DESELECT,
                        'organisation'  => WizardBehavior::BRANCH_SELECT,
                    ]
                );
                break;
            default:
                break;        
        }


        if ($event->step == "education" || $event->step == "qualification") {
            if (isset($post['Education'])) {
                $models = Model::createMultiple(Education::classname());
                //duplicate
            } else if (isset($post['Qualification'])) {
                $models = Model::createMultiple(Qualification::classname());
            }
            if (isset($post['cancel'])) {
                $event->continue = false;
            } elseif (isset($post['prev'])) {
                $event->nextStep = WizardBehavior::DIRECTION_BACKWARD;
                $event->handled  = true;
            } elseif (Model::loadMultiple($models, $post) && Model::validateMultiple($models)) {
                $event->data = $models;
                $event->handled = true;
                if (isset($post['pause'])) {
                    $event->continue = false;
                } elseif ($event->n < 2 && isset($post['add'])) {
                    $event->nextStep = WizardBehavior::DIRECTION_REPEAT;
                }
            } else {
                $event->data = $this->render($event->step, compact('event', 'models'));
            }

        } else {

            if (isset($post['cancel'])) {
                $event->continue = false;
            } elseif (isset($post['prev'])) {
                $event->nextStep = WizardBehavior::DIRECTION_BACKWARD;
                $event->handled  = true;
            } elseif ($model->load($post) && $model->validate()) {

                $event->data    = $model;
                $event->handled = true;
                if (isset($post['pause'])) {
                    $event->continue = false;
                } elseif ($event->n < 2 && isset($post['add'])) {
                    $event->nextStep = WizardBehavior::DIRECTION_REPEAT;
                }
            } else {
                $event->data = $this->render($event->step, compact('event', 'model'));
            }
        }
    }
    /**
    * @param WizardEvent The event
    */
    public function invalidStep($event)
    {
        $event->data = $this->render('invalidStep', compact('event'));
        $event->continue = false;
    }
    /**
    * Registration wizard has ended; the reason can be determined by the
    * step parameter: TRUE = wizard completed, FALSE = wizard did not start,
    * <string> = the step the wizard stopped at
    * @param WizardEvent The event
    */
    public function registrationAfterWizard($event)
    {
        if (is_string($event->step)) {
            $uuid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0x0fff) | 0x4000,
                mt_rand(0, 0x3fff) | 0x8000,
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
            );
            $registrationDir = Yii::getAlias('@runtime/project');
            $registrationDirReady = true;
            if (!file_exists($registrationDir)) {
                if (!mkdir($registrationDir) || !chmod($registrationDir, 0775)) {
                    $registrationDirReady = false;
                }
            }
            if ($registrationDirReady && file_put_contents(
                $registrationDir.DIRECTORY_SEPARATOR.$uuid,
                $event->sender->pauseWizard()
            )) {
                $event->data = $this->render('paused', compact('uuid'));
            } else {
                $event->data = $this->render('notPaused');
            }
        } elseif ($event->step === null) {
            $event->data = $this->render('cancelled');
        } elseif ($event->step) {
            

            $register = $event->stepData['register'];
            $salt = Encryption::generateSalt();
            $password = Encryption::encrypt($register[0]->password, $salt);
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            if ($register[0]->reg_type == 1) {
                try {
                    $individual = $event->stepData['individual'];
                    $education = $event->stepData['education'];
                    $qualification = $event->stepData['qualification'];
                    $upload = $event->stepData['upload'];
                
                    
                    $command = $connection->createCommand()->insert('user', [

                        'password' => $password,
                        'email' => $register[0]->email,
                        'salt' => $salt,
                        'statusId' => 1,
                        'role_id' => 2,
                        'date_created' => date('Y-m-d H:I:s'),
                    ]);

                    $command->execute();
                    $id = Yii::$app->db->getLastInsertID();
                    $i = 0;

                    $command = $connection->createCommand()->insert('user_details', [
                        'user_id' => $id,
                        'salutation_id' => $individual[0]->salutation,
                        'first_name' => $individual[0]->first_name,
                        'last_name' => $individual[0]->last_name,
                        'gender_id' => $individual[0]->gender,
                        'date_of_birth' => $individual[0]->date_of_birth,
                        'id_number' => $individual[0]->id_number,
                        'phone_number' => $individual[0]->phone_number,
                        'is_volunteer' => $individual[0]->is_volunteer,
                        'ward_id' => $individual[0]->ward,
                        'curriculum_vitae' => $upload[0]->curriculum_vitae,
                        'date_created' => date('Y-m-d H:I:s'),
                    ]);

                    $command->execute();
                    foreach ($education[0] as $edu) {
                        $command = $connection->createCommand()->insert('education', [
                            'user_id' => $id,
                            'degree_level_id' => $edu->degree_level,
                            'course_of_study_id' => $edu->course_of_study,
                            'date_created' => date('Y-m-d H:I:s'),
                        ]);

                        $command->execute();
                    }
                    foreach ($qualification[0] as $qua) {
                        $command = $connection->createCommand()->insert('qualification', [
                            'user_id' => $id,
                            'sector_id' => $qua->sector,
                            'organisation_name' => $qua->organisation_name,
                            'job_title' => $qua->job_title,
                            'years_experience' => $qua->years_experience,
                            'date_created' => date('Y-m-d H:I:s'),
                        ]);

                        $command->execute();
                    }
                    
                    $transaction->commit();
                    Yii::$app->session->setFlash('Success', 'Your details have been saved');
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Unable to save your registration details please try again later');
                }
                $event->data = $this->render('complete', [
                    'data' => $event->stepData,
                    'register' => $register[0],
                    'individual' => $individual[0],
                    'education' => $education[0],
                    'qualification' => $qualification[0],
                    'upload' => $upload[0],
                ]);
            } else  if ($register[0]->reg_type == 2) {
                try {
                    $individual = $event->stepData['individual'];
                    $company = $event->stepData['company'];
                    $profile = $event->stepData['profile'];
                
                    
                    $command = $connection->createCommand()->insert('user', [

                        'password' => $password,
                        'email' => $register[0]->email,
                        'salt' => $salt,
                        'statusId' => 1,
                        'role_id' => 3,
                        'date_created' => date('Y-m-d H:I:s'),
                    ]);

                    $command->execute();
                    $id = Yii::$app->db->getLastInsertID();
                    $i = 0;   
                    $command = $connection->createCommand()->insert('user_details', [
                        'user_id' => $id,
                        'salutation_id' => $individual[0]->salutation,
                        'first_name' => $individual[0]->first_name,
                        'last_name' => $individual[0]->last_name,
                        'gender_id' => $individual[0]->gender,
                        'date_of_birth' => $individual[0]->date_of_birth,
                        'id_number' => $individual[0]->id_number,
                        'phone_number' => $individual[0]->phone_number,
                        'is_volunteer' => $individual[0]->is_volunteer,
                        'ward_id' => $individual[0]->ward,
                        'curriculum_vitae' => $profile[0]->company_profile,
                        'date_created' => date('Y-m-d H:I:s'),
                    ]);

                    $command->execute();

                    $command = $connection->createCommand()->insert('company', [
                        'user_id' => $id,
                        'sector_id' => $company[0]->sector,
                        'company_name' => $company[0]->company_name,
                        'phone_number' => $company[0]->phone_number,
                        'address' => $company[0]->address,
                        'postal_code' => $company[0]->postal_code,
                        'city' => $company[0]->city,
                        'county_id' => $company[0]->county,
                        'email' => $company[0]->email,
                        'domain' => $company[0]->domain,
                        'date_created' => date('Y-m-d H:I:s'),
                    ]);

                    $command->execute();

                    $transaction->commit(); 
                    Yii::$app->session->setFlash('Success', 'Your details have been saved');
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Unable to save your registration details please try again later');
                }
                $event->data = $this->render('completed', [
                    'data' => $event->stepData,
                    'register' => $register[0],
                    'individual' => $individual[0],
                    'company' => $company[0],
                    'profile' => $profile[0],
                ]);   
            }
        } else {
            $event->data = $this->render('notStarted');
        }
    }
    /**
    * Method description
    *
    * @return mixed The return value
    */
    public function actionResume($uuid)
    {
        $registrationFile = Yii::getAlias('@runtime/project').DIRECTORY_SEPARATOR.$uuid;
        if (file_exists($registrationFile)) {
            $this->resumeWizard(@file_get_contents($registrationFile));
            unlink($registrationFile);
            $this->redirect(['project']);
        } else {
            return $this->render('notResumed');
        }
    }
}