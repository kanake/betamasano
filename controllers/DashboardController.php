<?php

namespace app\controllers;

use Yii;
use app\models\Action;
use app\models\ActionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * ActionController implements the CRUD actions for Action model.
 */
class DashboardController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            /*'access' => [
                'class' => AccessControl::className(),
                
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['dashboard'],
                        'roles' => ['dashboard'],
                    ],
                ],
            ],*/
        ];
    }


    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

}
